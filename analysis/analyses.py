from utils.utils import transpose
def nombre_octo_differents_au_34_sur_periode(solution):
    matrice_des_solutions = solution.valeur_des_variables_octo
    matrice_des_solutions = transpose(matrice_des_solutions)
    liste_des_octos_venant_sur_periode = []
    for ligne in matrice_des_solutions:

        if (sum(ligne) <= 0):
            liste_des_octos_venant_sur_periode.append(0)
        else:
            liste_des_octos_venant_sur_periode.append(1)

    return sum(liste_des_octos_venant_sur_periode)


def nombre_places_occupees_par_jour(solution):
    matrice_des_solutions = solution.valeur_des_variables_octo
    liste_des_octos_venant_par_jour = [sum(ligne) for ligne in matrice_des_solutions]
    return liste_des_octos_venant_par_jour