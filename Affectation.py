import random

from ortools.linear_solver import pywraplp
import matplotlib.pyplot as plt


def aggressive_random_input(number_of_employee, number_of_day_of_day_of_the_period=5, seed="OCTO"):
    """
    This function compute potential input data for the return to 34 problem.
    The input data genrated are made of a dictionary of potential presence for each employee and day and an array with the
    number of day each employee want to go to the office.

    The dictionary has in key a tuple of employee and day. It  has in value a boolean representing if the corresponding
    employee can come on the corresponding day.

    The number of day of the period is an array integers. Each integer represent number of day an employee
    want to go the office for each period.

    The input of this function is the number of employee and the number of day of the period for the problem.
    """
    random.seed(seed)
    dictionary_of_potential_presence_for_each_employee_and_day = {}
    for i in range(number_of_employee):
        for j in range(number_of_day_of_day_of_the_period):
            if random.random() > 0.6:
                dictionary_of_potential_presence_for_each_employee_and_day[i, j] = True
            else:
                dictionary_of_potential_presence_for_each_employee_and_day[i, j] = False
    number_of_day_each_employee_want_to_go_to_the_office = [random.randint(1, number_of_day_of_day_of_the_period)
                                                            for i in range(number_of_employee)]
    return dictionary_of_potential_presence_for_each_employee_and_day, \
           number_of_day_each_employee_want_to_go_to_the_office


def transpose(matrice):
    matrice_transposee = []
    for colonne in zip(*matrice):
        matrice_transposee.append(list(colonne))
    return matrice_transposee


def nombre_octo_differents_au_34_sur_periode(dico_solution):
    matrice_des_solutions = dico_solution['valeur_des_variables_octo']
    matrice_des_solutions = transpose(matrice_des_solutions)
    liste_des_octos_venant_sur_periode = []
    for ligne in matrice_des_solutions:

        if (sum(ligne) <= 0):
            liste_des_octos_venant_sur_periode.append(0)
        else:
            liste_des_octos_venant_sur_periode.append(1)

    return (sum(liste_des_octos_venant_sur_periode))


def nombre_places_occupees_par_jour(dico_solution):
    matrice_des_solutions = dico_solution['valeur_des_variables_octo']
    liste_des_octos_venant_par_jour = [sum(ligne) for ligne in matrice_des_solutions]
    return liste_des_octos_venant_par_jour


def maximisation_presence_personnes_differentes(nombre_octos, nombre_jours, capacite_max):
    dictionnaire_potentiel_presence, nombre_potentiel_de_jours = aggressive_random_input(nombre_octos,
                                                                                         number_of_day_of_day_of_the_period=nombre_jours)
    matrice_potentiel_presence = [[int(dictionnaire_potentiel_presence[(i, j)]) for i in range(nombre_octos)] for j in
                                  range(nombre_jours)]
    
    solver = pywraplp.Solver.CreateSolver('SCIP')
    
    # en colonne on a les variables octos et en ligne on a les variables jours
    matrice_variables_des_octos_jours = [
        [solver.IntVar(0.0, 1, 'octo ' + str(i) + ' jour ' + str(j)) for i in range(nombre_octos)] for j in
        range(nombre_jours)]
    
    # contrainte si un octo ne souhaite pas venir un jour sa variable = 0
    for octo in range(nombre_octos):
        for jour in range(nombre_jours):
            solver.Add(matrice_variables_des_octos_jours[jour][octo] <= matrice_potentiel_presence[jour][octo])
            
    # contrainte ne pas dépasser la capacité maximale
    for ligne in matrice_variables_des_octos_jours:
        solver.Add(sum(ligne) <= capacite_max)
    matrice_variables_des_octos_jours_transposee = transpose(matrice_variables_des_octos_jours)
    
    #variable qui prend 1 si l'octo vient dans la semaine sinon elle prend 0
    variable_est_present_dans_nb_jour = [solver.IntVar(0.0, 1, 'octo' + str(i)) for i in range(nombre_octos)]
    
    for octo, variable in zip(matrice_variables_des_octos_jours_transposee, variable_est_present_dans_nb_jour):
        solver.Add(sum(octo) <= (nombre_jours + 1) * variable)
        solver.Add(variable <= sum(octo))
        
    solver.Maximize(sum(variable_est_present_dans_nb_jour))
    
    status = solver.Solve()
    if status == pywraplp.Solver.OPTIMAL:
        fonction_objective_maximale = solver.Objective().Value()
        octos_est_present = [solution.solution_value() for solution in variable_est_present_dans_nb_jour]
        valeurs_des_solutions = [[solution.solution_value() for solution in ligne] for ligne in
                                 matrice_variables_des_octos_jours]
    # print('Solution:')
    # print('Objective value =', fonction_objective_maximale)
    # for ligne in valeurs_des_solutions:
    #   print(ligne)
    else:
        print('The problem does not have an optimal solution.')

    dico_solution = {'solution': fonction_objective_maximale, 'valeur_des_variables_octo': valeurs_des_solutions,
                     'temps_resolution': solver.wall_time()}
    return dico_solution


def maximisation_places_occupees(nombre_octos, nombre_jours, capacite_max):
    dictionnaire_potentiel_presence, nombre_potentiel_de_jours = aggressive_random_input(nombre_octos,
                                                                                         number_of_day_of_day_of_the_period=nombre_jours)
    
    matrice_potentiel_presence = [[int(dictionnaire_potentiel_presence[(i, j)]) for i in range(nombre_octos)] for j in
                                  range(nombre_jours)]
    
    solver = pywraplp.Solver.CreateSolver('SCIP')
    
    # en colonne on a les variables octos et en ligne on a les variables jours
    matrice_variables_des_octos_jours = [
        [solver.IntVar(0.0, 1, 'octo ' + str(i) + ' jour ' + str(j)) for i in range(nombre_octos)] for j in
        range(nombre_jours)]
    # contrainte si un octo ne souhaite pas venir un jour sa variable = 0
    for octo in range(nombre_octos):
        for jour in range(nombre_jours):
            solver.Add(matrice_variables_des_octos_jours[jour][octo] <= matrice_potentiel_presence[jour][octo])
    # contrainte ne pas dépasser la capacité maximale
    for ligne in matrice_variables_des_octos_jours:
        solver.Add(sum(ligne) <= capacite_max)
    matrice_variables_des_octos_jours_transposee = transpose(matrice_variables_des_octos_jours)
    variable_est_present_dans_nb_jour = [sum(presence_octo) for presence_octo in
                                        matrice_variables_des_octos_jours_transposee]
    
    solver.Maximize(sum(variable_est_present_dans_nb_jour))
    
    status = solver.Solve()
    if status == pywraplp.Solver.OPTIMAL:
        fonction_objective_maximale = solver.Objective().Value()
        octos_est_present = [solution.solution_value() for solution in variable_est_present_dans_nb_jour]
        valeurs_des_solutions = [[solution.solution_value() for solution in ligne] for ligne in
                                 matrice_variables_des_octos_jours]
    # print('Solution:')
    # print('Objective value =', fonction_objective_maximale)
    # for ligne in valeurs_des_solutions:
    #    print(ligne)
    else:
        print('The problem does not have an optimal solution.')

    dico_solution = {'solution': fonction_objective_maximale, 'valeur_des_variables_octo': valeurs_des_solutions,
                     'temps_resolution': solver.wall_time()}
    return dico_solution


def main():
    capacite_max = 500
    capacite_max_semaine =[capacite_max*5 for i in range(10)]
    variable_nombre_octo = [i for i in range(100, 1100, 100)]
    temps_resolution1 = [
        maximisation_presence_personnes_differentes(nombre_octos, nombre_jours=5, capacite_max=capacite_max)['temps_resolution'] for
        nombre_octos in variable_nombre_octo]
    temps_resolution2 = [
        maximisation_places_occupees(nombre_octos, nombre_jours=5, capacite_max=capacite_max)['temps_resolution'] for
        nombre_octos in variable_nombre_octo]
    print(variable_nombre_octo)
    print(temps_resolution1)
    print(temps_resolution2)

    liste_du_nombre_octo_different_fonction_effectif1 = [nombre_octo_differents_au_34_sur_periode(
        maximisation_presence_personnes_differentes(nombre_octos, nombre_jours=5, capacite_max=capacite_max)) for nombre_octos in
        variable_nombre_octo]
    liste_du_nombre_octo_different_fonction_effectif2 = [nombre_octo_differents_au_34_sur_periode(
        maximisation_places_occupees(nombre_octos, nombre_jours=5, capacite_max=capacite_max)) for nombre_octos in
        variable_nombre_octo]

    liste_des_places_occupees_1 = [
        sum(nombre_places_occupees_par_jour(
            maximisation_presence_personnes_differentes(nombre_octos, nombre_jours=5, capacite_max=capacite_max)))
        for nombre_octos in variable_nombre_octo]

    liste_des_places_occupees_2 = [
        sum(nombre_places_occupees_par_jour(maximisation_places_occupees(nombre_octos, nombre_jours=5, capacite_max=capacite_max)))
        for nombre_octos in variable_nombre_octo]

    plt.figure(1)
    plt.plot(variable_nombre_octo, liste_du_nombre_octo_different_fonction_effectif1, 'o--',
             label="maximisation du nombre d'individus")
    plt.plot(variable_nombre_octo, liste_du_nombre_octo_different_fonction_effectif2, 'o--',
             label="maximisation des places occupées")
    plt.xlabel("nombre d'octos")
    plt.plot(variable_nombre_octo, variable_nombre_octo, 'o--', label="nombre d'octos")
    plt.grid(axis='x', color='0.95')
    plt.legend()
    plt.title("nombre d'octos différents se prénsentant au 34 sur la semaine")


    plt.figure(2)
    plt.plot(variable_nombre_octo, liste_des_places_occupees_1, linestyle='solid', marker='o',
             label="maximisation du nombre d'individus")
    plt.plot(variable_nombre_octo, liste_des_places_occupees_2, linestyle='solid', marker='o',
             label="maximisation des places occupées")
    plt.plot(variable_nombre_octo,capacite_max_semaine,linestyle='--',label="capacité du 34 sur la semaine")
    plt.grid(axis='x', color='0.95')
    plt.xlabel("nombre d'octos")
    plt.legend()
    plt.title('nombres de places occupées au 34 sur la semaine')


    plt.figure(3)
    plt.plot(variable_nombre_octo,temps_resolution1, label="maximisation du nombre d'individus")
    plt.plot(variable_nombre_octo,temps_resolution2, label="maximisation des places occupées")
    plt.grid(axis='x', color='0.95')
    plt.legend()
    plt.title('temps de résolution du problème en millisecondes ')

    plt.show()


if __name__ == '__main__':
    main()
