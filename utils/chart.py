import matplotlib.pyplot as plt


def trichart_grided(numfig, abscisses, liste1, liste2, liste3, xlabel='', ylabel='', label1='', label2='', label3='',
                    type='o--', title=''):
    plt.figure(numfig)
    plt.plot(abscisses, liste1,type, label=label1)
    plt.plot(abscisses, liste2,type, label=label2)
    plt.plot(abscisses, liste3,type, label=label3)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.grid(axis='x', color='0.95')
    plt.legend()
    plt.title(title)


def bichart(numfig, abscisses, liste1, liste2, xlabel='', ylabel='', label1='', label2='', title=''):
    plt.figure(numfig)
    plt.plot(abscisses, liste1, label=label1)
    plt.plot(abscisses, liste2, label=label2)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.title(title)
