def transpose(matrice):
    matrice_transposee = []
    for colonne in zip(*matrice):
        matrice_transposee.append(list(colonne))
    return matrice_transposee