def dico_to_matrix(dictionnaire_potentiel_presence, nombre_jours, nombre_octos):
    matrice_potentiel_presence = [[int(dictionnaire_potentiel_presence[(i, j)]) for i in range(nombre_octos)] for j in
                                  range(nombre_jours)]
    return matrice_potentiel_presence