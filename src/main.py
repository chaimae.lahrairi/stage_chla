from utils.utils_instance import dico_to_matrix
from instance.retour_au_34_optimization import *
from instance.input_retour_au_34 import *
from utils.chart import *


def main():
    capacite_max = 500
    nombre_jours = 5
    capacite_max_semaine = [capacite_max * 5 for i in range(10)]
    variable_nombre_octo = [i for i in range(100, 1100, 100)]
    # TODO : refactor
    list_matrices_potentiel_presence = [
        dico_to_matrix(aggressive_random_input(nombre_octos, number_of_day_of_day_of_the_period=nombre_jours)[0],
                       nombre_jours, nombre_octos) for nombre_octos in variable_nombre_octo]

    matrice_nb_jour_max_souhaite = [
        aggressive_random_input(nombre_octos, number_of_day_of_day_of_the_period=nombre_jours)[1] for nombre_octos in
        variable_nombre_octo]


    liste_du_nombre_octo_different_fonction_effectif1 = [nombre_octo_differents_au_34_sur_periode(
        maximisation_presence_personnes_differentes(nombre_octos, nombre_jours, capacite_max,
                                                    matrice_potentiel_presence, list_nb_jour_max_souhaite))
        for nombre_octos, matrice_potentiel_presence, list_nb_jour_max_souhaite in
        zip(variable_nombre_octo, list_matrices_potentiel_presence,matrice_nb_jour_max_souhaite)]

    liste_du_nombre_octo_different_fonction_effectif2 = [nombre_octo_differents_au_34_sur_periode(
        maximisation_places_occupees(nombre_octos, nombre_jours, capacite_max, matrice_potentiel_presence,
                                     list_nb_jour_max_souhaite))
        for nombre_octos, matrice_potentiel_presence, list_nb_jour_max_souhaite in
        zip(variable_nombre_octo, list_matrices_potentiel_presence, matrice_nb_jour_max_souhaite)]

    liste_des_places_occupees_1 = [
        sum(nombre_places_occupees_par_jour(
            maximisation_presence_personnes_differentes(nombre_octos, nombre_jours, capacite_max,
                                                        matrice_potentiel_presence, list_nb_jour_max_souhaite)))
        for nombre_octos, matrice_potentiel_presence, list_nb_jour_max_souhaite in
        zip(variable_nombre_octo, list_matrices_potentiel_presence,matrice_nb_jour_max_souhaite)]

    liste_des_places_occupees_2 = [
        sum(nombre_places_occupees_par_jour(
            maximisation_places_occupees(nombre_octos, nombre_jours, capacite_max,
                                         matrice_potentiel_presence, list_nb_jour_max_souhaite)))
        for nombre_octos, matrice_potentiel_presence, list_nb_jour_max_souhaite in
        zip(variable_nombre_octo, list_matrices_potentiel_presence,matrice_nb_jour_max_souhaite)]

    temps_resolution1 = [
        maximisation_presence_personnes_differentes(nombre_octos, nombre_jours, capacite_max,
                                                    matrice_potentiel_presence,
                                                    list_nb_jour_max_souhaite).temps_resolution
        for nombre_octos, matrice_potentiel_presence, list_nb_jour_max_souhaite in
        zip(variable_nombre_octo, list_matrices_potentiel_presence, matrice_nb_jour_max_souhaite)]
    temps_resolution2 = [
        maximisation_places_occupees(nombre_octos, nombre_jours, capacite_max,
                                     matrice_potentiel_presence, list_nb_jour_max_souhaite).temps_resolution
        for nombre_octos, matrice_potentiel_presence, list_nb_jour_max_souhaite in
        zip(variable_nombre_octo, list_matrices_potentiel_presence, matrice_nb_jour_max_souhaite)]

    trichart_grided(1, variable_nombre_octo, liste_du_nombre_octo_different_fonction_effectif1,
                    liste_du_nombre_octo_different_fonction_effectif2,
                    variable_nombre_octo,
                    xlabel='nombre_octos',
                    label1="maximisation du nombre d'individus",
                    label2="maximisation des places occupées",
                    label3="nombre d'octos",
                    title="nombre d'octos différents se prénsentant au 34 sur la semaine")

    trichart_grided(2, variable_nombre_octo, liste_des_places_occupees_1,
                    liste_des_places_occupees_2,
                    capacite_max_semaine,
                    xlabel='nombre_octos',
                    label1="maximisation du nombre d'individus",
                    label2="maximisation des places occupées",
                    label3="capacité du 34 sur la semaine",
                    title='nombres de places occupées au 34 sur la semaine')

    bichart(3, variable_nombre_octo, temps_resolution1,
            temps_resolution2,
            xlabel='temps en ms',
            label1="maximisation du nombre d'individus",
            label2="maximisation des places occupées",
            title='temps de résolution du problème en millisecondes ')

    plt.show()


if __name__ == '__main__':
    main()
