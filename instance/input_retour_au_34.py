import random
from utils.utils_instance import dico_to_matrix
from utils.utils import transpose


def aggressive_random_input(number_of_employee, number_of_day_of_day_of_the_period=5, seed="OCTO"):
    """
    This function compute potential input data for the return to 34 problem.
    The input data genrated are made of a dictionary of potential presence for each employee and day and an array with the
    number of day each employee want to go to the office.

    The dictionary has in key a tuple of employee and day. It  has in value a boolean representing if the corresponding
    employee can come on the corresponding day.

    The number of day of the period is an array integers. Each integer represent number of day an employee
    want to go the office for each period.

    The input of this function is the number of employee and the number of day of the period for the problem.
    """
    random.seed(seed)
    dictionary_of_potential_presence_for_each_employee_and_day = {}
    for i in range(number_of_employee):
        for j in range(number_of_day_of_day_of_the_period):
            if random.random() > 0.6:
                dictionary_of_potential_presence_for_each_employee_and_day[i, j] = True
            else:
                dictionary_of_potential_presence_for_each_employee_and_day[i, j] = False
    #####################################-CHLA-######################################################
    matrice_souhait_presence = dico_to_matrix(dictionary_of_potential_presence_for_each_employee_and_day,number_of_day_of_day_of_the_period,number_of_employee)
    matrice_souhait_presence_transposee = transpose(matrice_souhait_presence)
    #################################################################################################
    number_of_day_each_employee_want_to_go_to_the_office = [random.randint(0, sum(souhait_presence))
                                                            for i, souhait_presence in zip(range(number_of_employee),
                                                                                           matrice_souhait_presence_transposee)]
    return dictionary_of_potential_presence_for_each_employee_and_day, \
           number_of_day_each_employee_want_to_go_to_the_office
