from ortools.linear_solver import pywraplp

from solver.contraintes import *
from solver.variables import *

from analysis.analyses import *
from solver.resolve import resolution
from collections import namedtuple


def maximisation_presence_personnes_differentes(nombre_octos, nombre_jours, capacite_max,
                                                matrice_potentiel_presence,list_nb_jour_max_souhaite):
    solver = pywraplp.Solver.CreateSolver('SCIP')

    # en colonne on a les variables octos et en ligne on a les variables jours
    matrice_variables_des_octos_jours = declaration_variables_octo_jour(solver, nombre_octos, nombre_jours)

    # contrainte si un octo ne souhaite pas venir un jour sa variable = 0
    mettre_variables_a_zero(solver, nombre_octos, nombre_jours, matrice_variables_des_octos_jours,
                            matrice_potentiel_presence)

    # contrainte ne pas dépasser la capacité maximale
    ne_pas_depasser_cap_max(solver, capacite_max, matrice_variables_des_octos_jours)

    matrice_variables_des_octos_jours_transposee = transpose(matrice_variables_des_octos_jours)

    # contrainte respecter le nombre de jours maximum souhaité pat chaque octo
    respecter_nombre_jours_max_souhaite(solver, list_nb_jour_max_souhaite, matrice_variables_des_octos_jours)

    # variable qui va prend 1 si l'octo vient dans la semaine sinon elle prend 0
    # TODO :refactor
    variable_est_present_dans_nb_jour = [solver.IntVar(0.0, 1, 'octo' + str(i)) for i in range(nombre_octos)]

    for octo, variable in zip(matrice_variables_des_octos_jours_transposee, variable_est_present_dans_nb_jour):
        solver.Add(sum(octo) <= (nombre_jours + 1) * variable)
        solver.Add(variable <= sum(octo))

    solver.Maximize(sum(variable_est_present_dans_nb_jour))

    fonction_objective_maximale, valeurs_des_solutions = resolution(solver, matrice_variables_des_octos_jours)

    tuple_solution = namedtuple('tuple_solution', ['valeur_optimale', 'valeur_des_variables_octo', 'temps_resolution'])
    solution = tuple_solution(fonction_objective_maximale, valeurs_des_solutions, solver.wall_time())
    return solution


def maximisation_places_occupees(nombre_octos, nombre_jours, capacite_max, matrice_potentiel_presence,list_nb_jour_max_souhaite):
    solver = pywraplp.Solver.CreateSolver('SCIP')

    # en colonne on a les variables octos et en ligne on a les variables jours
    matrice_variables_des_octos_jours = declaration_variables_octo_jour(solver, nombre_octos, nombre_jours)

    # contrainte si un octo ne souhaite pas venir un jour sa variable = 0
    mettre_variables_a_zero(solver, nombre_octos, nombre_jours, matrice_variables_des_octos_jours,
                            matrice_potentiel_presence)
    # contrainte ne pas dépasser la capacité maximale
    ne_pas_depasser_cap_max(solver, capacite_max, matrice_variables_des_octos_jours)

    # contrainte respecter le nombre de jours maximum souhaité pat chaque octo
    respecter_nombre_jours_max_souhaite(solver, list_nb_jour_max_souhaite, matrice_variables_des_octos_jours)

    matrice_variables_des_octos_jours_transposee = transpose(matrice_variables_des_octos_jours)
    variable_est_present_dans_nb_jour = [sum(presence_octo) for presence_octo in
                                         matrice_variables_des_octos_jours_transposee]

    solver.Maximize(sum(variable_est_present_dans_nb_jour))

    fonction_objective_maximale, valeurs_des_solutions = resolution(solver, matrice_variables_des_octos_jours)
    tuple_solution = namedtuple('tuple_solution', ['valeur_optimale', 'valeur_des_variables_octo', 'temps_resolution'])
    solution = tuple_solution(fonction_objective_maximale, valeurs_des_solutions, solver.wall_time())
    return solution
