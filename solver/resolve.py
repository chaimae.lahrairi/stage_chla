from ortools.linear_solver import pywraplp
def resolution(solver,matrice_variables_des_octos_jours):
    status = solver.Solve()
    if status == pywraplp.Solver.OPTIMAL:
        fonction_objective_maximale = solver.Objective().Value()

        valeurs_des_solutions = [[solution.solution_value() for solution in ligne] for ligne in
                                 matrice_variables_des_octos_jours]
    else:
        print('The problem does not have an optimal solution.')

    return fonction_objective_maximale, valeurs_des_solutions