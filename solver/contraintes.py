from utils.utils import transpose


def mettre_variables_a_zero(solver, nombre_octos, nombre_jours, matrice_variables_des_octos_jours,
                            matrice_potentiel_presence):
    for octo in range(nombre_octos):
        for jour in range(nombre_jours):
            solver.Add(matrice_variables_des_octos_jours[jour][octo] <= matrice_potentiel_presence[jour][octo])


def ne_pas_depasser_cap_max(solver, capacite_max, matrice_variables_des_octos_jours):
    for ligne in matrice_variables_des_octos_jours:
        solver.Add(sum(ligne) <= capacite_max)


def respecter_nombre_jours_max_souhaite(solver, list_nb_jour_max_souhaite, matrice_variables_des_octos_jours):
    matrice_variables_des_octos_jours_transposee = transpose(matrice_variables_des_octos_jours)
    for colonne, nb_max_souhait in zip(matrice_variables_des_octos_jours_transposee, list_nb_jour_max_souhaite):
        solver.Add(sum(colonne) <= nb_max_souhait)
