def declaration_variables_octo_jour(solver, nombre_octos, nombre_jours):
    matrice_variables_des_octos_jours = [
        [solver.IntVar(0.0, 1, 'octo ' + str(i) + ' jour ' + str(j)) for i in range(nombre_octos)] for j in
        range(nombre_jours)]
    return matrice_variables_des_octos_jours