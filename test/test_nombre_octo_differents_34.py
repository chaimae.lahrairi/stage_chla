import unittest
from collections import namedtuple
from analysis.analyses import nombre_octo_differents_au_34_sur_periode


class TestNombreOctoDifferentsAu34SurPeriode(unittest.TestCase):
    def test_basic_case(self):
        tuple_test = namedtuple('tuple_test', ['valeur_des_variables_octo'])
        matrice_test = [[i % 2 for i in range(5)] for j in range(2)]
        solution = tuple_test(matrice_test)

        self.assertEqual(nombre_octo_differents_au_34_sur_periode(solution), 2)


if __name__ == '__main__':
    unittest.main()
