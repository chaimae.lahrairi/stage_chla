import unittest
from instance.retour_au_34_optimization import maximisation_presence_personnes_differentes
from ortools.linear_solver import pywraplp
from utils.utils import transpose
import random

nombre_octos = 10
nombre_jours = 5
cap_max = 3

matrice_de_souhait = [[random.randint(0, 1) for i in range(nombre_octos)] for j in range(nombre_jours)]
list_nb_jours_max = [random.randint(0, 3) for i in range(nombre_octos)]
solution = maximisation_presence_personnes_differentes(nombre_octos, nombre_jours, cap_max, matrice_de_souhait,
                                                       list_nb_jours_max)

# TODO: une seule classe test
# TODO : refactor given/when/then
#expected result
class TestMettreVariableaZero(unittest.TestCase):
    def test_basic_case(self):

        liste_souhait_moins_solution = [[souhait - solution for souhait, solution in
                                         zip(liste_des_octo_souhaitant_venir, liste_des_solutions)] for
                                        liste_des_octo_souhaitant_venir, liste_des_solutions in
                                        zip(matrice_de_souhait, solution.valeur_des_variables_octo)]

        for liste in liste_souhait_moins_solution:
            for element in liste:
                self.assertTrue(element >= 0)
                # add assertion here


class NePasDepasserCapMax(unittest.TestCase):
    def test_basic_case(self):
        for ligne in solution.valeur_des_variables_octo:
            self.assertTrue(sum(ligne) <= cap_max)


class RespecterNombreJoursSouhaite(unittest.TestCase):
    def test_basic_case(self):
        matrice_des_solutions_transposee = transpose(solution.valeur_des_variables_octo)
        for colonne, element  in zip(matrice_des_solutions_transposee, list_nb_jours_max):
            self.assertTrue(sum(colonne) <= element)


if __name__ == '__main__':
    unittest.main()
